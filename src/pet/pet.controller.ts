// Nest
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';

// Object
import { PetService } from './pet.service';
import { CreatePetDto } from './dtos/create-pet.dto';
import { UpdatePetDto } from './dtos/update-pet.dto';

@Controller('coolblue/api/pets')
export class PetController {
  constructor(private petService: PetService) {}

  @Get('/:id')
  findPetById(@Param('id') id: string) {
    return this.petService.findOneById(parseInt(id));
  }

  @Get()
  findAllPets(@Query('species') species: string) {
    return this.petService.findAll(species);
  }

  @Post()
  createPet(@Body() body: CreatePetDto) {
    this.petService.create(body);
  }

  @Patch('/:id')
  updatePet(@Param('id') id: string, @Body() body: UpdatePetDto) {
    this.petService.update(parseInt(id), body);
  }

  @Delete('/:id')
  deletePet(@Param('id') id: string) {
    this.petService.remove(parseInt(id));
  }
}
