import { IsBoolean, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreatePetDto {
  @IsString()
  name: string;

  @IsString()
  species: string;

  @IsOptional()
  @IsBoolean()
  available: boolean;

  @IsOptional()
  @IsNumber()
  birthYear: number;

  @IsOptional()
  @IsString()
  dateAdded: string;

  @IsOptional()
  @IsString()
  photoUrl: string;
}
