import { IsBoolean, IsOptional, IsString, IsNumber } from 'class-validator';

export class UpdatePetDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsString()
  species: string;

  @IsOptional()
  @IsBoolean()
  available: boolean;

  @IsOptional()
  @IsNumber()
  birthYear: number;

  @IsOptional()
  @IsString()
  dateAdded: string;

  @IsOptional()
  @IsString()
  photoUrl: string;
}
