// Nest
import { Logger } from '@nestjs/common';

// Typeorm
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  AfterInsert,
  AfterUpdate,
  AfterRemove,
} from 'typeorm';

@Entity()
export class Pet {
  private readonly logger = new Logger(Pet.name);

  // Identification
  @PrimaryGeneratedColumn()
  id: number;

  // Required info
  @Column()
  name: string;

  @Column()
  species: string;

  // General info
  @Column({ nullable: true })
  available: boolean;

  @Column({ nullable: true })
  photoUrl: string;

  // Dates
  @Column({ nullable: true })
  birthYear: number;

  @Column({ nullable: true })
  dateAdded: string;

  // Hooks decorators
  @AfterInsert()
  LogInsertItem() {
    this.logger.log(`Item is created in DB with ID:${this.id}`);
  }

  @AfterUpdate()
  LogUpdateItem() {
    this.logger.log(`Item is updated in DB with ID:${this.id}`);
  }

  @AfterRemove()
  LogRemoveItem() {
    this.logger.log(`Item is removed from DB!`);
  }
}
