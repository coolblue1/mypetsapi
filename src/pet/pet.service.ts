// Nest
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

// TypeOrm
import { Repository } from 'typeorm';

// Object
import { Pet } from './pet.entity';

@Injectable()
export class PetService {
  constructor(@InjectRepository(Pet) private repo: Repository<Pet>) {}

  create(data: Partial<Pet>): Promise<Pet> {
    const createData = Object.assign({}, data);
    const pet = this.repo.create(createData);

    return this.repo.save(pet);
  }

  async findOneById(id: number): Promise<Pet> {
    if (!id) return null;
    return await this.repo.findOneBy({ id });
  }

  async findAll(species: string): Promise<Pet[]> {
    return await this.repo.find({ where: { species } });
  }

  async update(id: number, data: Partial<Pet>): Promise<Pet> {
    const pet = await this.findOneById(id);

    if (!pet) throw new NotFoundException('Pet not found!');

    Object.assign(pet, data);

    return this.repo.save(pet);
  }

  async remove(id: number): Promise<Pet> {
    const pet = await this.findOneById(id);

    if (!pet) throw new NotFoundException('Pet not found!');

    return this.repo.remove(pet);
  }
}
