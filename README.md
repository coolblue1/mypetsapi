# Introduction

Welcome to the Coolblue Assignment - Pet API. Here, you'll discover information to guide you on testing and verifying this assignment!

## Application

- Clone the repository from this [link](https://gitlab.com/coolblue1/mypetsapi/-/tree/main?ref_type=heads).
- [Install NodeJs/NPM](https://websnl.atlassian.net/wiki/spaces/IDT/pages/2365390879/NPM+Node.js)
- Install NestJS on your computer: `npm install -g @nestjs/cli`
- Run `npm install` to install npm packages.
- Either `nest start` or `npm run start:dev` will run the application

Please note that the moment you run the application, a file-based database named `db.sqlite` will be created in the root of your folder. This file stores the data and enhances the testing experience for the project! 😍

## Test & Verify

You can install [Postman](https://www.postman.com/) on your computer and while the app is running test the API. All resources may be called from this bae URI: `http://localhost:3000/coolblue/api/pets`

But if you are using "Visual Studio Code" I suggest a much eaiser way like this.

- Install [REST Clinet Api](https://marketplace.visualstudio.com/items?itemName=donebd.rest-client-api) extension on your Visual studio code. You can easily go to "VS Code Extensions Marketplace" and install it!

- Open `src/pet/requests` folder, and you'll see all the endpoints are there, ready to be called!

- If you install the mentioned extension, you'll see "Send Request" button above each endpoint!

- If it is not visible, please reaload your VS Code window!

- Use this [link](https://share.vidyard.com/watch/VtSGVBSt5tP1QXVwuK81G4) for a visual guide!

### How Can I see the data inside databse?

- Please install [SQLite](https://marketplace.visualstudio.com/items?itemName=alexcvzz.vscode-sqlite) `v0.14.1` extension on VS Code! You can easily go to "VS Code Extensions Marketplace" and install it!
- On VS Code please run `ctrl + shift + p` and then execute command `SQLite: Open Database`

- Select `db.sqlite` file
- A new window will be added to the left-side of your VS Code!
- There, you can open the database and see the databse in the table!
- If you see nothing, please first add data to the API, and then check it again!
- Use this [link](https://share.vidyard.com/watch/9t8ui63KAcx7kdepEFLbrB) for a visual guide!

If none of these worked, utilize the API itself for fetching the data. 😉

Happy Testing! ⌨️💻🖥️
